﻿DesertPlaceholder
=================
Animated placeholder in desert style

## ScreenShot
<img src="https://gitee.com/openharmony-tpc/desertplaceholder/raw/master/art/desertplaceholder.gif" width="40%"/>

## Gradle
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:desertPlaceholder:1.0.2'
```

## Usage
-----

Add component to your layout
``` xml
  <com.jetradar.desertplaceholder.DesertPlaceholder
      ohos:id="$+id:placeholder"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:dp_buttonText="Retry"
      ohos:dp_message="Use this nice placeholder if you have nothing to show"/>
```

Set up listener to button click
``` java
    DesertPlaceholder desertPlaceholder = (DesertPlaceholder) findComponentById(ResourceTable.Id_placeholder);
    desertPlaceholder.setOnButtonClickListener(new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
             // do stuff
        }
    });
```

## entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## License
-------

    Copyright 2016 JetRadar

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

