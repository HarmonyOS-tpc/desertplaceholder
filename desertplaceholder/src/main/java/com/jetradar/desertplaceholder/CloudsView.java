/*
 * Copyright (C) 2016 JetRadar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jetradar.desertplaceholder;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.ArrayList;
import java.util.List;

import static com.jetradar.desertplaceholder.DesertPlaceholder.animationEnabled;

public class CloudsView extends Component implements Component.DrawTask {

    private static final float SPEED_DP_PER_SEC = 20f;
    private final List<Cloud> clouds = new ArrayList<>();

    private Paint paint;
    private PixelMap cloudTop;
    private PixelMap cloudMiddle;
    private PixelMap cloudBottom;
    private float density;
    private double timeStamp = -1;
    private int cloudMiddleHeight;

    public CloudsView(Context context) {
        super(context);
        init(context);
    }

    public CloudsView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context);
    }

    public CloudsView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        paint = new Paint();
        cloudTop = BitmapFactory.decodeResource(getContext(), ResourceTable.Media_cloud3);
        cloudMiddle = BitmapFactory.decodeResource(getContext(), ResourceTable.Media_cloud2);
        cloudBottom = BitmapFactory.decodeResource(getContext(), ResourceTable.Media_cloud1);
        density = context.getResourceManager().getDeviceCapability().screenDensity / 160;
        cloudMiddleHeight = cloudMiddle.getImageInfo().size.height;
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        long time = System.currentTimeMillis();
        if (timeStamp != -1) {
            drawClouds(canvas, time);
        } else {
            initClouds();
        }
        timeStamp = time;
        if (animationEnabled) {
            getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    invalidate();
                }
            });
        }
    }

    private void initClouds() {
        int width = getWidth();
        int height = getHeight();
        clouds.add(new Cloud(cloudTop, 0.3f, 0));
        clouds.add(new Cloud(cloudMiddle, 0.6f, height / 2 - cloudMiddleHeight / 2));
        clouds.add(new Cloud(cloudBottom, 0.8f, height - cloudMiddleHeight));
        float percent = 0.1f;
        for (Cloud cloud : clouds) {
            cloud.x = width * percent;
            percent += 0.25f;
        }
    }

    private void drawClouds(Canvas canvas, long time) {
        for (Cloud cloud : clouds) {
            updatePosition(cloud, (time - timeStamp) / 1000d);
            canvas.drawPixelMapHolder(new PixelMapHolder(cloud.bitmap), cloud.x, cloud.y, paint);
        }
    }

    private void updatePosition(Cloud cloud, double timeDelta) {
        cloud.x += density * SPEED_DP_PER_SEC * cloud.speedMultiplier * timeDelta;
        int width = getWidth();
        if (cloud.x > width) {
            cloud.x = -cloud.bitmap.getImageInfo().size.width;
        }
    }

    private static class Cloud {
        final PixelMap bitmap;
        final float speedMultiplier;
        final int y;
        float x;

        Cloud(PixelMap bitmap, float speedMultiplier, int y) {
            this.bitmap = bitmap;
            this.speedMultiplier = speedMultiplier;
            this.y = y;
        }
    }

}